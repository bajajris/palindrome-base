package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Invalid palindrome word", Palindrome.isPalindrome("racecar"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Invalid palindrome word", Palindrome.isPalindrome(null));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Invalid palindrome word", Palindrome.isPalindrome("edit tide"));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Invalid palindrome word", Palindrome.isPalindrome("edit on tide"));
	}	
	
}
